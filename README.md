### ES6 Summary Notes  

1. Summary:  
  ES6 (ECMAScript 6) is a release version of a standard guide for browser running javascript.  
2. Setting up:
  * Not all of browsers support ES6, so es6 need to be transpiling to es5.
  * The common compiler is Babel.  
  * To using babel, we can:  
  ```
    - Attach babel through cdnjs: https://cdnjs.cloudflare.com/ajax/libs/babel-core/6.1.19/browser.min.js  
      + <script type="text/babel">
    or  
    - Using babel and babel-node
      + npm: npm i babel
      => babel <target_file>.js --watch --out-file <compiled_file>.js
      => babel-node <target_file>.js
      NOTE:
      - npm: npm install --save-dev babel-preset-env (this helps babel know which version should compile to)
    - Using webpack
      + npm: npm i -D webpack babel-loader
      + configure:
        module.exports = {
          entry: './script.js',
          output: { filenmae: 'bundle.js' },
          module: {
            rules: [
              {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: {
                  loader: 'babel-loader',
                  options: {
                    presets: ['env']
                  }
                }
              }
            ]
          }
        }
  ```
3. Features:   
  1. Var vs Let/Const   
    ```  
    - The main different between all of keyword here:
      + Var = using function scope
      + Let/Const = using block scope ({...})  
      Const differs from let keyword by not allow to reassign value.  
    ```  
  2. Template strings:  
    ```
    - syntax: `${variable}`
    - usage: using instead of plus string sequences.  
    ```  
  3. Spread operators:  
  ```
    - syntax: ...
    - usage: return new array, obj (NOT SHADOW-CLONING)
    - example:
      + myFunction(...iterableObj) instad of myFunction.apply(null, args)
      + [...iterableObj]
      + let objClone = {...obj}
  ```  
  4. Default function parameters:  
  ```  
    - syntax: myFunction(parameters1 = value1, ...)  
    - usage: when no parameters put into function, function will get default values instead.
  ```  
  5. Enhancing object literals:  
  ```  
    - example:
      var obj = {
        func: function(arg) {
          ....
        }
      }  
      convert to:  
      var obj = {
        func(arg) {
          ....
        }
      }
  ```
  6. Arrow functions:  
  ```  
    - syntax: (args) => {}  
    - usage: this belongs the lift up scope instead of function scope.
  ```   
  7. DEstructuring assignment:  
  ```  
    - syntax: [var1,,,var2] = ['val1', 'val2', 'val3', 'val4']; // var1 = val1, var2 = val2  
    - usage: use with both array, object.
  ```  
  8. Generators:  
  ```
    - syntax: function* aFunction() {
      yield;
    }
    - keyword:
      + .next() to call next 'yield';
      + value: to get value
      + .done: return boolean to know whether finshed or not
  ```  
  9. Class syntax:  
  ```  
    - syntax:
      class C {
        constructor(args) { ... }

        func() {}
      }

      => var instance = new C (..)

      class E extends C {
        constructor() {
          super(...)
        }
      }
  ```  
